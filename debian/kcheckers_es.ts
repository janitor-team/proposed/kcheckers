<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>myHistory</name>
    <message>
        <source>Move</source>
        <translation>Mover</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation>Deshacer</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation>Rehacer</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>Continuar</translation>
    </message>
    <message>
        <source>Set Comment</source>
        <translation>Definir un comentario</translation>
    </message>
    <message>
        <source>Set Tag</source>
        <translation>Definir una etiqueta</translation>
    </message>
    <message>
        <source>Tag</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <source>Reading file...</source>
        <translation>Leyendo el fichero...</translation>
    </message>
    <message>
        <source>Importing games...</source>
        <translation>Importando los juegos</translation>
    </message>
    <message>
        <source>English draughts</source>
        <translation>Damas inglesas</translation>
    </message>
    <message>
        <source>Russian draughts</source>
        <translation>Damas rusas</translation>
    </message>
    <message>
        <source>Unknown game type</source>
        <translation>Tipo de juego desconocido</translation>
    </message>
    <message>
        <source>Free Placement Mode</source>
        <translation>Modo de movimiento libre</translation>
    </message>
    <message>
        <source>Paused Mode</source>
        <translation>Modo de pausa</translation>
    </message>
    <message>
        <source>Play Mode</source>
        <translation>Modo de juego</translation>
    </message>
</context>
<context>
    <name>myHumanPlayer</name>
    <message>
        <source>Go!</source>
        <translation type="obsolete">Commencer !</translation>
    </message>
    <message>
        <source>Incorrect course.</source>
        <translation type="obsolete">Mouvement impossible. Peut-être une capture est-elle possible ?</translation>
    </message>
    <message>
        <source>You must capture.</source>
        <translation>Es obligatorio capturar la ficha</translation>
    </message>
    <message>
        <source>Cannot move this.</source>
        <translation>No es possible hacer este movimiento.</translation>
    </message>
</context>
<context>
    <name>myInfo</name>
    <message>
        <source>Move</source>
        <translation type="obsolete">Déplacer</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation type="obsolete">Commentaire</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation type="obsolete">Refaire</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="obsolete">Continuer</translation>
    </message>
    <message>
        <source>Set Comment</source>
        <translation type="obsolete">Définir le Commentaire</translation>
    </message>
    <message>
        <source>Set Tag</source>
        <translation type="obsolete">Définir le Tag</translation>
    </message>
    <message>
        <source>Reading file...</source>
        <translation type="obsolete">Lecture du fichier en cours...</translation>
    </message>
    <message>
        <source>Importing games...</source>
        <translation type="obsolete">Importe les jeux...</translation>
    </message>
    <message>
        <source>English draughts</source>
        <translation type="obsolete">Dames anglaises</translation>
    </message>
    <message>
        <source>Russian draughts</source>
        <translation type="obsolete">Dames russes</translation>
    </message>
    <message>
        <source>Unknown game type</source>
        <translation type="obsolete">Type de jeu inconnu</translation>
    </message>
    <message>
        <source>Free Placement Mode</source>
        <translation type="obsolete">Mode de Placement Libre</translation>
    </message>
    <message>
        <source>Paused Mode</source>
        <translation type="obsolete">Mode Pause</translation>
    </message>
    <message>
        <source>Play Mode</source>
        <translation type="obsolete">Mode Jeu</translation>
    </message>
    <message>
        <source>Tag</source>
        <translation type="obsolete">Tag</translation>
    </message>
</context>
<context>
    <name>myNewGameDlg</name>
    <message>
        <source>New Game</source>
        <translation>Juego nuevo</translation>
    </message>
    <message>
        <source>Against CPU on this PC</source>
        <translation type="obsolete">Contre l&apos;ordinateur local</translation>
    </message>
    <message>
        <source>Against Human on Network - New Game</source>
        <translation type="obsolete">Contre un humain en réseau - Nouvelle partie</translation>
    </message>
    <message>
        <source>Against Human on Network - Join Game</source>
        <translation type="obsolete">Contre un humain en réseau - Joindre une partie</translation>
    </message>
    <message>
        <source>Rules</source>
        <translation>Reglas</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="obsolete">Anglaises</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="obsolete">Russes</translation>
    </message>
    <message>
        <source>Skill</source>
        <translation>Nivel de dificultad</translation>
    </message>
    <message>
        <source>Beginner</source>
        <translation>Principiante</translation>
    </message>
    <message>
        <source>Novice</source>
        <translation>Iniciado</translation>
    </message>
    <message>
        <source>Average</source>
        <translation>Medio</translation>
    </message>
    <message>
        <source>Good</source>
        <translation>Bueno</translation>
    </message>
    <message>
        <source>Expert</source>
        <translation>Experto</translation>
    </message>
    <message>
        <source>Master</source>
        <translation>Maestro</translation>
    </message>
    <message>
        <source>Server IP:</source>
        <translation type="obsolete">IP du serveur:</translation>
    </message>
    <message>
        <source>Pick free port</source>
        <translation type="obsolete">Prendre un port libre</translation>
    </message>
    <message>
        <source>&amp;Start</source>
        <translation>&amp;Iniciar</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>C&amp;ancelar</translation>
    </message>
    <message>
        <source>Human</source>
        <translation>Humano</translation>
    </message>
    <message>
        <source>Player One</source>
        <translation>Un jugador</translation>
    </message>
    <message>
        <source>White</source>
        <translation>Blancas</translation>
    </message>
    <message>
        <source>Player Two</source>
        <translation>Dos jugadores</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation>Ordenador</translation>
    </message>
    <message>
        <source>Free Men Placement</source>
        <translation>Movimento libre de las damas</translation>
    </message>
</context>
<context>
    <name>myTopLevel</name>
    <message>
        <source>&amp;New...</source>
        <translation>&amp;Neuvo...</translation>
    </message>
    <message>
        <source>CTRL+N</source>
        <comment>File|New</comment>
        <translation>CTRL+N</translation>
    </message>
    <message>
        <source>&amp;Next Round</source>
        <translation>&amp;Siguiente nivel</translation>
    </message>
    <message>
        <source>&amp;Stop</source>
        <translation>&amp;Parar</translation>
    </message>
    <message>
        <source>&amp;Undo Move</source>
        <translation type="obsolete">&amp;Annuler le mouvement</translation>
    </message>
    <message>
        <source>CTRL+Z</source>
        <comment>File|Undo</comment>
        <translation type="obsolete">CTRL+Z</translation>
    </message>
    <message>
        <source>&amp;Information</source>
        <translation type="obsolete">&amp;Information</translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation>&amp;Abrir...</translation>
    </message>
    <message>
        <source>CTRL+O</source>
        <comment>File|Open</comment>
        <translation>CTRL+O</translation>
    </message>
    <message>
        <source>&amp;Save...</source>
        <translation>&amp;Guardar...</translation>
    </message>
    <message>
        <source>CTRL+S</source>
        <comment>File|Save</comment>
        <translation>CTRL+S</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <source>CTRL+Q</source>
        <comment>File|Quit</comment>
        <translation>CTRL+Q</translation>
    </message>
    <message>
        <source>&amp;Show Notation</source>
        <translation>&amp;Ver la anotación</translation>
    </message>
    <message>
        <source>&amp;Green Board</source>
        <translation type="obsolete">&amp;Plateau vert</translation>
    </message>
    <message>
        <source>&amp;Marble Board</source>
        <translation type="obsolete">&amp;Plateau marbré</translation>
    </message>
    <message>
        <source>&amp;Wooden Board</source>
        <translation type="obsolete">&amp;Plateau en bois</translation>
    </message>
    <message>
        <source>What&apos;s This</source>
        <translation type="obsolete">Qu&apos;est ce que c&apos;est ?</translation>
    </message>
    <message>
        <source>SHIFT+F1</source>
        <comment>Help|WhatsThis</comment>
        <translation type="obsolete">SHIFT+F1</translation>
    </message>
    <message>
        <source>&amp;Rules of Play</source>
        <translation>&amp;Reglas del juego</translation>
    </message>
    <message>
        <source>F1</source>
        <comment>Help|Help</comment>
        <translation>F1</translation>
    </message>
    <message>
        <source>&amp;About </source>
        <translation type="obsolete">À &amp;propos </translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation>Créditos de &amp;Qt</translation>
    </message>
    <message>
        <source>&amp;Game</source>
        <translation>&amp;Juego</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation>&amp;Configurar KCheckers</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <source>Save Game</source>
        <translation>Guardar la partida</translation>
    </message>
    <message>
        <source>Could not save: </source>
        <translation>No es posible guardar: </translation>
    </message>
    <message>
        <source>Open Game</source>
        <translation>Cargar una partida</translation>
    </message>
    <message>
        <source>Could not load: </source>
        <translation type="obsolete">Impossible d&apos;ouvrir: </translation>
    </message>
    <message>
        <source>Game Info</source>
        <translation type="obsolete">Informations sur la partie</translation>
    </message>
    <message>
        <source>Rules of Play</source>
        <translation>Reglas del juego</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Créditos</translation>
    </message>
    <message>
        <source>Quit Game?</source>
        <translation type="obsolete">Quitter la partie ?</translation>
    </message>
    <message>
        <source>Current game will be lost if you continue.
Do you really want to discard it?</source>
        <translation>El juego actual se perderá si continua.
¿Seguro que quiere descartar la partida?</translation>
    </message>
    <message>
        <source>Abort Game?</source>
        <translation>¿Abandonar el juego ?</translation>
    </message>
    <message>
        <source>&amp;Confirm aborting current game</source>
        <translation>&amp;Confirmar el abandono del jego actual</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;Créditos</translation>
    </message>
    <message>
        <source>Show notation &amp;above men</source>
        <translation>Ver la notación &amp;en las fichas</translation>
    </message>
    <message>
        <source>Clear &amp;log on new round</source>
        <translation>Borrar el &amp;log en la nueva partida</translation>
    </message>
    <message>
        <source>&amp;Notation font...</source>
        <translation>Tipode letra en la nueva partida...</translation>
    </message>
    <message>
        <source>&amp;Toolbar</source>
        <translation>&amp;Barra de herramientas</translation>
    </message>
    <message>
        <source>&lt;p&gt;In the beginning of game you have 12 checkers (men). The men move forward only. The men can capture:&lt;ul&gt;&lt;li&gt;by jumping forward only (english rules);&lt;li&gt;by jumping forward or backward (russian rules).&lt;/ul&gt;&lt;p&gt;A man which reaches the far side of the board becomes a king. The kings move forward or backward:&lt;ul&gt;&lt;li&gt;to one square only (english rules);&lt;li&gt;to any number of squares (russian rules).&lt;/ul&gt;&lt;p&gt;The kings capture by jumping forward or backward. Whenever a player is able to make a capture he must do so.</source>
        <translation>&lt;p&gt;Al principio, usted tiene 12 fichas. Las fichas se muven hacia delante. Puede capturar otras fichas:&lt;ul&gt;&lt;li&gt; solo saltando hacia delante (raglas inglesas),&lt;li&gt;saltando en los dos sentidos (reglas rusas).&lt;/ul&gt;&lt;p&gt;Si una ficha llega al lado contrario del tablero se transforma en una dama. Las damas pueden desplazarse en los dos sentidos:&lt;ul&gt;&lt;li&gt;solo una casilla (raglas inglesas),&lt;li&gt;más de una casilla (reglas rusas).&lt;/ul&gt;&lt;p&gt;Las damas pueden capturar saltando en los dos sentidos. La captura de una ficha siempre es obligatoria.
</translation>
    </message>
</context>
<context>
    <name>myView</name>
    <message>
        <source>Go!</source>
        <translation type="obsolete">Commencer !</translation>
    </message>
    <message>
        <source>You have lost. Game over.</source>
        <translation type="obsolete">Vous avez perdu. Fin de la partie.</translation>
    </message>
    <message>
        <source>Congratulation! You have won!</source>
        <translation type="obsolete">Félicitations ! Vous avez gagné !</translation>
    </message>
    <message>
        <source>I am thinking...</source>
        <translation type="obsolete">Je pense...</translation>
    </message>
    <message>
        <source>Waiting network player to move...</source>
        <translation type="obsolete">En attente du mouvement du joueur distant...</translation>
    </message>
    <message>
        <source>Incorrect course.</source>
        <translation type="obsolete">Mouvement impossible. Peut-être une capture est-elle possible ?</translation>
    </message>
    <message>
        <source>Waiting for network player to connect...</source>
        <translation type="obsolete">En attente de la connexion du joueur distant...</translation>
    </message>
    <message>
        <source>Waiting for server to reply...</source>
        <translation type="obsolete">En attente de la réponse du serveur...</translation>
    </message>
    <message>
        <source>Game aborted.</source>
        <translation>Partida abandonada</translation>
    </message>
    <message>
        <source>ENGLISH rules.</source>
        <translation type="obsolete">Règles ANGLAISES.</translation>
    </message>
    <message>
        <source>RUSSIAN rules.</source>
        <translation type="obsolete">Règles RUSSES.</translation>
    </message>
    <message>
        <source>
New Network Game</source>
        <translation type="obsolete">
Nouvelle partie en réseau</translation>
    </message>
    <message>
        <source>
Join Network Game</source>
        <translation type="obsolete">
Rejoindre une partie en réseau</translation>
    </message>
    <message>
        <source>Unknown rules. Playing current rules: </source>
        <translation type="obsolete">Règles inconnues. Adopte les règles courantes :</translation>
    </message>
    <message>
        <source>Unknown skill. Keeping current skill: </source>
        <translation type="obsolete">Niveau inconnu. Conserve le niveau courant :</translation>
    </message>
    <message>
        <source>Player</source>
        <translation type="obsolete">Le joueur</translation>
    </message>
    <message>
        <source>is played by KCheckers with current skill: </source>
        <translation type="obsolete">est controllé par KCheckers avec le niveau courant :</translation>
    </message>
    <message>
        <source>This is not implemented yet.</source>
        <translation type="obsolete">Ceci n&apos;est pas encore implémenté.</translation>
    </message>
    <message>
        <source>Please consider Information mismatch.</source>
        <translation type="obsolete">Veuillez considérer que les informations sont discordantes.</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="obsolete">Russes</translation>
    </message>
    <message>
        <source>Drawn game.</source>
        <translation>Empate.</translation>
    </message>
    <message>
        <source>Invalid move.</source>
        <translation>Movimient erróneo.</translation>
    </message>
    <message>
        <source>White wins!</source>
        <translation>¡Las blancas han vencido!</translation>
    </message>
    <message>
        <source>Black wins!</source>
        <translation>¡Las negras han vencido!</translation>
    </message>
    <message>
        <source>opponent</source>
        <translation type="obsolete">adversaire</translation>
    </message>
    <message>
        <source>Opened:</source>
        <translation>Abierto:</translation>
    </message>
    <message>
        <source>Warning! Some errors occured.</source>
        <translation>¡Atención! Se han producido algunos errores.</translation>
    </message>
    <message>
        <source>Saved:</source>
        <translation>Guardado:</translation>
    </message>
</context>
</TS>
